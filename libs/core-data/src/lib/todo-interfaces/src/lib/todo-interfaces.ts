export interface ToDo {
  id?: string,
  title: string,
  description: string,
  urgency?: number,
  createdAt?: string
}
