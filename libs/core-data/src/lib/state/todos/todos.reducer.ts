import { ToDo } from '@test-nx-repo/core-data/todo-interfaces';

const initialTodos: ToDo[] = [{
  title: 'Project Twelve',
  description: 'Will start soon',
  urgency: 1,
  createdAt: '18.8.2021',
  id: 'qM6HI0M'
},
  {
    title: 'Project One',
    description: 'urgent',
    urgency: 2,
    createdAt: '18.8.2021',
    id: 'yZc6-6l'
  }]

// Shape of state
export interface TodosState {
  todos: ToDo[];
  selectedTodoId: string | null;
}

// initial state
export const initialState: TodosState = {
  todos: initialTodos,
  selectedTodoId: null
}

// build reducer
export function todosReducer(state = initialState, action):TodosState{
  switch(action.type){
    default:
      return state
  }
}
