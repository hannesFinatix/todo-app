import * as fromTodos from './todos/todos.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface AppState {
  todos: fromTodos.TodosState
}

export const reducers: ActionReducerMap<AppState> = {
  todos: fromTodos.todosReducer
}
