import { Component } from '@angular/core';
import {ToDo} from '@test-nx-repo/core-data/todo-interfaces';
import { TodosService } from './shared/todos.service';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'test-nx-repo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'To-Do-App';

  todos$: Observable<ToDo[]> = of([]);
  selectedToDo: ToDo = {
    title: '',
    description: ''
  }

  constructor(private todoServices: TodosService) {
  }

  ngOnInit(): void {
    this.loadAllToDos()
    this.resetTodo()
  }

  selectToDo(todo: ToDo) {
    this.selectedToDo = todo
    console.log('It is selected! ', this.selectedToDo)
  }

  loadAllToDos () {
   this.todos$ = this.todoServices.getAllToDos()
  }

  saveToDo(todo: ToDo): void {
    console.log(todo);
    if (todo.id){
      this.updateToDo(todo)
    } else {
      this.todoServices.addToDo(todo).pipe(
        tap(() => this.loadAllToDos())
      ).subscribe();
    }
    this.resetTodo();
  }
  updateToDo(todo: ToDo) {
    this.todoServices.updateToDo(todo).pipe(
      tap(() => this.loadAllToDos())
    ).subscribe();
  }



  deleteTodo(id: string){
    console.log("App says: delete number", id)
    this.todoServices.deleteToDo(id).pipe(
      tap(() => this.loadAllToDos())
    ).subscribe();
  }

  resetTodo(){
    return this.selectedToDo = {
      title: '',
      description: '',
    }
  }
}
