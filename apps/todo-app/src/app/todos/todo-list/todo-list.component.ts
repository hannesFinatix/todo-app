import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ToDo } from '@test-nx-repo/core-data/todo-interfaces';
@Component({
  selector: 'test-nx-repo-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {

  @Input() todos: ToDo[] | null =[];
  @Output() deleteToDo = new EventEmitter<string>();
  @Output() todoSelected = new EventEmitter<ToDo>()

  selectToDo(todo: ToDo){
    this.todoSelected.emit(todo)
  }
  deletedToDo(id: string | undefined){
    this.deleteToDo.emit(id)
  }
}
