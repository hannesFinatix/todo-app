import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TodosComponent } from './todos.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { RouterModule } from '@angular/router';
import { TodosRoutingModule } from './todos-routing.module';


@NgModule({
  declarations: [
    TodoListComponent,
    TodoFormComponent,
    TodosComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    TodosRoutingModule
  ],

  exports: [
    TodosComponent
  ]
})
export class TodosModule {

}
