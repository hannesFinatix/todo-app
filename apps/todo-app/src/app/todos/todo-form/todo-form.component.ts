import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ToDo } from '@test-nx-repo/core-data/todo-interfaces';

@Component({
  selector: 'test-nx-repo-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {

  currentToDo: ToDo = {
    title: '',
    description: ''
  }
  @Input() set selectedToDo (value: ToDo) {
    this.currentToDo = Object.assign({}, value)
  }
  @Output() savedToDo = new EventEmitter<ToDo>();
  @Output() resetForm = new EventEmitter<ToDo>()

  constructor() { }

  ngOnInit(): void {
  }

  saveTodo(todo: ToDo){
    this.savedToDo.emit(todo);
  }

  clearForm(){
    this.resetForm.emit()
  }

}
