import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToDo } from '@test-nx-repo/core-data/todo-interfaces';

const BASE_URL = 'http://localhost:3000/todos/';
const currentDate = new Date().toLocaleDateString();
@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private http: HttpClient) { }

  getAllToDos(){
    return this.http.get<ToDo[]>(BASE_URL)
  }

  addToDo(todo: ToDo) {
    return this.http.post<ToDo>(BASE_URL, Object.assign(todo, {createdAt: currentDate}));
  }

  updateToDo(todo: ToDo){
    return this.http.put<ToDo>(BASE_URL + todo.id, todo)
  }

  deleteToDo(id: string){
    return this.http.delete<ToDo>(BASE_URL + id)
  }
}
